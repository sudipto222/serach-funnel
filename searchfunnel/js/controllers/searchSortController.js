serviceCenterApp.controller('searchSortCtrl',function($document, $scope,$http,$filter) {    
'use strict';

/* Initialization of variables */
function init(){
	$scope.filteredSearchData = [];
	$scope.searchResults = [];
	$scope.typeList = [];
	$scope.search = {};
	$scope.sortKey = 'Person.PersonNamings[0].FirstName';
	$scope.reverse  = false;
	$scope.currentPage = 1;
	$scope.numPerPage = 14;
	$scope.maxSize = 5;
}

function getSearchData(){
	/* Getting results from mock json */
	$http.get("json/mock.json").success(function(response){ 
        $scope.searchResults = response.BusinessPartnerList;

		$scope.typeList  = $scope.searchResults.reduce(function(list, item) {
			if(list.indexOf(item.BusinessPartnerType.BusinessPartnerTypeCode) == -1) {
				list.push(item.BusinessPartnerType.BusinessPartnerTypeCode);
			}
			return list;
		}, []);

		$scope.search.type = angular.copy($scope.typeList);
		
		/* Pagination watch */

		var watchImpact = function() {
			var begin = (($scope.currentPage - 1) * $scope.numPerPage)
			, end = begin + $scope.numPerPage;

			var preFilters = $filter('searchFilter')($scope.searchResults, $scope.search);
			preFilters = $filter('orderBy')(preFilters, $scope.sortKey, $scope.reverse);
			$scope.totalItems = preFilters.length;
			$scope.filteredSearchData = preFilters.slice(begin, end);
		}

		$scope.$watch('currentPage + numPerPage', watchImpact);
		$scope.$watch('search.name', watchImpact);
		$scope.$watch('search.street', watchImpact);
		$scope.$watch('search.postCode', watchImpact);
		$scope.$watch('search.city', watchImpact);
		$scope.$watch('search.dOB', watchImpact);
		$scope.$watch('search.type.length', watchImpact);
		$scope.$watch('search.taxID', watchImpact);
		$scope.$watch('search.siret', watchImpact);
		$scope.$watch('search.bPID', watchImpact);
		$scope.$watch('search.match', watchImpact);
		$scope.$watch('reverse', watchImpact);
		$scope.$watch('sortKey', watchImpact);
	});
};

$scope.pageChanged = function(){
window.scrollTo(0,0);

};

$(document).on('mouseenter', ".dob", function() {
     var $this = $(this);
     if(this.offsetWidth < this.scrollWidth && !$this.attr('title')) {
          $this.tooltip({
               title: $this.text(),
               placement: "bottom"
          });
          $this.tooltip('show');
     }
});

/* Sorting values according to columns */
$scope.sort = function(keyname){
     $scope.sortKey = keyname;
     $scope.reverse = !$scope.reverse;
     $scope.selectBox();
}

$scope.box = 0;
$scope.tmp = 0;
$scope.selectBox = function(setTab){
	if( $scope.tmp != setTab ) {
		$scope.tmp = setTab;
		$scope.box = setTab;
	} else {
		$scope.box = 0;
		$scope.tmp = 0;
	}
}
$scope.isSelectedBox = function(checkTab){
	return $scope.box === checkTab;
}

$scope.test = function() {
	console.log($scope.search);
}

$(document).click(function() {
	$scope.$apply(function(){
		$scope.selectBox();
	});
});

 
$scope.updateClass = function(field, type) {
	var newClasses = "";

	try {
		switch(type) {
			case "string":
				newClasses = $scope.search.hasOwnProperty(field) && $scope.search[field].trim() != "" ? "color-blue": "";
				break;
			case "array":
				newClasses = $scope.search.hasOwnProperty(field) && $scope.search[field].length > 0 ? "color-blue": "";
				break;
		}
	} catch(e) {
		console.log($scope.search);
		console.log(e);
	} finally {
		return newClasses;
	}
}

$('.tms-ellipsis, .tms-box').click(function(event){
	event.stopPropagation();
});

init();
getSearchData();
});
