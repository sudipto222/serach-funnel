serviceCenterApp.filter('searchFilter', function () {
	'use strict';
	var stringFilter = function(list, getEleFrmLst, comapre) {
		return list.reduce(function(newList, item) {
			var i1 = getEleFrmLst(item);
			if(comapre == undefined || (i1 != undefined && i1.toLowerCase().indexOf(comapre.toLowerCase())!== -1))
					newList.push(item);
			return newList;
		}, []);
	}

	var checkListFilter = function(list, getEleFrmLst, comapre) {
		return list.reduce(function(newList, item) {
			if(comapre.indexOf(getEleFrmLst(item)) !== -1)
				newList.push(item);
			return newList;
		}, []);
	}

	var params = {
		name: function(i) {
			return i.Person.PersonNamings[0].FirstName +' '+ i.Person.PersonNamings[0].LastName;
		},
		street: function(i) { return i.LocationAddresses[0].PostalAddress.StreetLine +' '+ i.LocationAddresses[0].PostalAddress.StreetNumber; },
		postCode: function(i) { return i.LocationAddresses[0].PostalAddress.PostalCode; },
		city: function(i) { return i.LocationAddresses[0].PostalAddress.City; },
		dOB: function(i) { return i.Person.DateOfBirth; },
		type: function(i) { return i.BusinessPartnerType.BusinessPartnerTypeCode;; },
		taxID: function(i) { return i.TaxInformations[0].TaxInformationIdentifier; },
		siret: function(i) { return i.Person.Siret; },
		bPID: function(i) { return i.BusinessPartnerIdentifier; },
		match: function(i) { return i.Person.Match; }
	};

	return function (list, search) {
		// console.log(search);
		// var list = JSON.parse(JSON.stringify(results));
		var keys = Object.keys(search);
		for (var i in keys) {
			var key = keys[i];
			list = key == 'type'? checkListFilter(list, params[key], search[key]): stringFilter(list, params[key], search[key]);
		}
		return list;
	}

});









