serviceCenterApp.filter('searchFilter', function () {
'use strict';
    return function (results,nameValue,streetVal,postcodeVal,cityVal,dobVal,typeVal,taxIdVal,siretVal,bpidVal,matchVal) {
			var matches = [];
			
			if(!!nameValue){/**@case1 if only name query is present**/
				for (var i = 0; i < results.length; i++) {
					var name = results[i].Person.PersonNamings[0].FirstName +' '+ results[i].Person.PersonNamings[0].LastName;
					if (name.toLowerCase().indexOf(nameValue.toLowerCase())!== -1) {
						matches.push(results[i]);
					}
				}
			}		
			 else if(!!streetVal){ /**@case2 if only street query is present**/
				for (var i = 0; i < results.length; i++) {
					var streetName = results[i].LocationAddresses[0].PostalAddress.StreetLine +' '+ results[i].LocationAddresses[0].PostalAddress.StreetNumber;
					if (streetName.toLowerCase().indexOf(streetVal.toLowerCase())!== -1) {
						matches.push(results[i]);
					}
				}
			} 
			else if(!!postcodeVal){ /**@case3 if only postcode query is present**/
				for (var i = 0; i < results.length; i++) {
					var postalCode = results[i].LocationAddresses[0].PostalAddress.PostalCode;
					if (postalCode.toLowerCase().indexOf(postcodeVal.toLowerCase())!== -1) {
						matches.push(results[i]);
					}
				}
			}
			else if(!!cityVal){ /**@case4 if only city query is present**/
				for (var i = 0; i < results.length; i++) {
					var city = results[i].LocationAddresses[0].PostalAddress.City;
					if (city.toLowerCase().indexOf(cityVal.toLowerCase())!== -1) {
						matches.push(results[i]);
					}
				}
			}
			else if(!!dobVal){/**@case5 if only dob query is present**/
				for (var i = 0; i < results.length; i++) {
					var dob = results[i].Person.DateOfBirth;
					if (dob.toLowerCase().indexOf(dobVal.toLowerCase())!== -1) {
						matches.push(results[i]);
					}
				}
			}
			else if(!!typeVal){/**@case6 if only type query is present**/
				for (var i = 0; i < results.length; i++) {
					var type = results[i].BusinessPartnerType.BusinessPartnerTypeCode;
					if (typeVal.indexOf(type) != -1) {
					// if (type.toLowerCase().indexOf(typeVal.toLowerCase())!== -1) {
						matches.push(results[i]);
					}
				}
			}
			else if(!!taxIdVal){/**@case7 if only taxid query is present**/
				for (var i = 0; i < results.length; i++) {
					var tax_id = results[i].TaxInformations[0].TaxInformationIdentifier;
					if (taxIdVal.indexOf(tax_id) != -1) {
						if (tax_id.toLowerCase().indexOf(taxIdVal.toLowerCase())!== -1) {
							matches.push(results[i]);
						}
					}
				}
			}
			else if(!!siretVal){/**@case8 if only siret query is present**/
				for (var i = 0; i < results.length; i++) {
					var siret = results[i].Person.Siret;
					if (siret.toLowerCase().indexOf(siretVal.toLowerCase())!== -1) {
						matches.push(results[i]);
					}
				}
			}
			else if(!!bpidVal){/**@case9 if only bpid query is present**/
				for (var i = 0; i < results.length; i++) {
					var bpid = results[i].BusinessPartnerIdentifier;
					if (bpid.toLowerCase().indexOf(bpidVal.toLowerCase())!== -1) {
						matches.push(results[i]);
					}
				}
			}
			else if(!!matchVal){/**@case10 if only match% query is present**/
				for (var i = 0; i < results.length; i++) {
					var match = results[i].Person.Match;
					if (match.toLowerCase().indexOf(matchVal.toLowerCase())!== -1) {
						matches.push(results[i]);
					}
				}
			}
			else {
            /** case 11 no query is present**/
				matches = results;
			}
			return matches;
		
	}
});









