serviceCenterApp
    .directive("checkboxGroup", function() {
        "use strict"
        return {
            restrict: "A",
            link: function(scope, elem, attrs) {
                // Determine initial checked boxes
                if (scope.search.type.indexOf(scope.item) !== -1) {
                    elem[0].checked = true;
                }

                // Update scope.search.typeay on click
                elem.bind('click', function() {
                    var index = scope.search.type.indexOf(scope.item);
                    // Add if checked
                    if (elem[0].checked) {
                        if (index === -1) scope.search.type.push(scope.item);
                    }
                    // Remove if unchecked
                    else {
                        if (index !== -1) scope.search.type.splice(index, 1);
                    }
                    // Sort and update DOM display
                    scope.$apply(scope.search.type.sort(function(a, b) {
                        return a - b
                    }));
                });
            }
        }
    });